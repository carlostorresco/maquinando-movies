<footer class="footer">
    <div class="footer-copyright text-center py-3">© <?php echo date("Y") ?> Copyright</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>