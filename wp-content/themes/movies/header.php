<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<!-- DNS prefetching -->
	<link rel="dns-prefetch" href="//www.google-analytics.com">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<!-- Website info -->
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<!-- Google Fonts -->

	<!-- WP Head -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!--<a class="skip-link screen-reader-text" href="#content"><?php _e('Skip to content', 'twentynineteen'); ?></a>-->
	<!-- Content here -->
	<header class="header site-header">
		<div class="container-fluid">
			<!--Navbar-->
			<nav class="navbar navbar-expand-lg">
				<!-- Navbar brand -->
				<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">Maquinando <small>Movies</small></a>
				<!-- Collapse button -->
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<!-- Collapsible content -->
				<?php if ( has_nav_menu( 'menu-header' ) ) : ?>
					<div class="collapse navbar-collapse" aria-label="<?php esc_attr_e( 'Top Menu', 'maquinando' ); ?>">	
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'menu-header',
								'menu_class'     => 'navbar-nav ml-auto',
								'depth' => 2,
								'container' => '',
								'container_class' => ''
							)
						);
						?>
					</div>
					<!-- /.Collapsible content -->
				<?php endif; ?>
			</nav>
			<!--/.Navbar-->
		</div>
	</header>