<?php
get_header();
?>
<main id="main" class="site-main">
    <div class="container-fluid">
    <?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
				the_post();
				get_template_part( 'partials/content/content' );
			}
		}
		?>
    </div>
</main>
<?php
get_footer();
