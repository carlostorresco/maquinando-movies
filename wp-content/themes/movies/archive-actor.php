<?php
get_header();

$args = array(
    'post_type' => 'actor',
    'orderby'   => 'rand',
    'posts_per_page' => 3, 
    );
 
$featured = new WP_Query( $args );

//var_dump($featured);

?>
<main id="main" class="site-main">
    <div class="container-fluid">
        <section class="featured-list m-auto">
            <h4 class="featured-list--title">Actores destacados</h4>
            <div class="row">
            <?php 
                if ( $featured->have_posts() ) {
                    while ( $featured->have_posts() ) {
                        $featured->the_post();
                        $score = get_post_meta(get_the_ID(), 'input_score', true);?>
                        <div class="col-sm">
                            <div class="card">
                                <a href="<?php the_permalink() ?>">
                                    <img class="card-img-top" src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title"><?php the_title() ?></h5>
                                    <a href="<?php the_permalink() ?>" class="btn view-more">Ver más</a>
                                </div>
                            </div>
                        </div>
            <?php   }
                }
            ?>
            </div>
            <div class="row">
                <div class="col-sm">
                    <a href="#" class="btn all-movies">Todos los actores</a>
                </div>
            </div>
        </section>
        <section class="maquinando-list">
            <div class="row">
            <?php
            if (have_posts()) :
                while (have_posts()) :the_post();
                    get_template_part('partials/content/content', 'actor');
                endwhile;
            endif;
            ?>
            </div>
        </section>
    </div>
</main>
<?php
get_footer();
