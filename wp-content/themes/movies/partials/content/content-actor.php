<?php
/**
 * Template part for displaying posts
 */

$score = get_post_meta(get_the_ID(), 'input_score', true); 

?>
<div class="col-12 col-sm-4 col-md-3">
    <article id="post-<?php the_ID(); ?>" <?php post_class('card'); ?>>
        <a href="<?php the_permalink() ?>">
            <img class="card-img-top" src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
        </a>
        <div class="card-body">
            <h5 class="card-title"><?php the_title() ?></h5>
            <a href="<?php the_permalink() ?>" class="btn view-more">Ver más</a>
        </div>
    </article>
</div>