<?php

/**
 * Template part for displaying posts
 */

$movies = get_post_meta($post->ID, 'input_movie_id', true);
$characters = get_post_meta($post->ID, 'input_character', true);

$args = array(
    'post_type' => 'movie',
    'post__in' => $movies,
    'posts_per_page' => -1
);

$movies_list = new WP_Query($args);

?>
<section class="view-detail m-auto">
    <div class="media">
        <div class="media-left">
            <img class="media-object" src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
        </div>
        <div class="media-body">
            <h4 class="media-heading"><?php the_title() ?></h4>
            <div class="media-content"><?php the_content() ?></div>
            <?php if ($movies_list->have_posts()) : ?>
                <div class="characters">
                    <span>Películas:</span>
                    <?php while ($movies_list->have_posts()) :
                        $movies_list->the_post(); ?>
                        <a href="<?php the_permalink() ?>" class="characters--actor"><?php the_title() ?></a>
                    <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>