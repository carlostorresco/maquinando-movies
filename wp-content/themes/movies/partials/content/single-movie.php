<?php

/**
 * Template part for displaying posts
 */

$score = get_post_meta(get_the_ID(), 'input_score', true);
$actors = get_post_meta($post->ID, 'input_actor', true);

$args = array(
    'post_type' => 'actor',
    'post__in' => $actors,
    'posts_per_page' => -1
);

$actors_list = new WP_Query($args);

?>
<section class="view-detail m-auto">
    <div class="media">
        <div class="media-left">
            <img class="media-object" src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
        </div>
        <div class="media-body">
            <h4 class="media-heading"><?php the_title() ?></h4>
            <div class="media-content"><?php the_content() ?></div>
            <div class="score">
                <span>Calificación:</span>
                <span class="number">
                    <?php echo $score ?>
                    <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" class="ipc-icon ipc-icon--star-inline" viewBox="0 0 24 24" fill="currentColor" role="presentation">
                        <path d="M12 20.1l5.82 3.682c1.066.675 2.37-.322 2.09-1.584l-1.543-6.926 5.146-4.667c.94-.85.435-2.465-.799-2.567l-6.773-.602L13.29.89a1.38 1.38 0 0 0-2.581 0l-2.65 6.53-6.774.602C.052 8.126-.453 9.74.486 10.59l5.147 4.666-1.542 6.926c-.28 1.262 1.023 2.26 2.09 1.585L12 20.099z"></path>
                    </svg>
                </span>
            </div>
            <?php if ($actors_list->have_posts() && $actors) : ?>
                <div class="characters">
                    <span>Actores:</span>
                    <?php while ($actors_list->have_posts()) :
                        $actors_list->the_post(); ?>
                        <a href="<?php the_permalink() ?>" class="characters--actor"><?php the_title() ?></a>
                    <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>