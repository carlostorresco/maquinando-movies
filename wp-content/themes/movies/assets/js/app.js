const appMaquinando = (() => {
    const $ = jQuery;

    const init = () => {
        fnViewallInfo();
    }

    var fnViewallInfo = () => {
        $('.all-movies').on('click', function(event) {
            event.preventDefault();
            $('.featured-list').fadeOut(100);
            $('.maquinando-list').fadeIn(200);
        });
    }

    return { init: init };

})();


jQuery(document).ready(function($) {
    appMaquinando.init();
});