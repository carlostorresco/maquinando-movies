jQuery(document).ready(function($) {
    console.log('::: Maquinando :::');

    $('#addMovie').on('click', function() {
        let movie = $('#input_movie'),
            currentMovie = movie.val();


        console.log('currentMovie', currentMovie);

        var contentHTML = `
            <div class="list_movies--row">
                <input type="hidden" name="input_movie_id[]" value="${currentMovie}" />
                <div class="list_movies--movie">
                    <span class="title">Película:</span> 
                    <span class="name">${$.trim(movie.text())}</span>
                </div>
                <div class="list_movies--character">
                    <span class="title">Nombre del personaje:</span> 
                    <input type="text" name="input_character[]" placeholder="Nombre del personaje" />
                </div>
                <a href="javascript:;" id="removeCharacter" onclick="fnDeleteCharacter(this)" class="list_movies--delete"><i class="dashicons dashicons-no"></i></a>
            </div>
        `;

        $('.list_movies').append(contentHTML);

    })

    $('#removeCharacter').on('click', function(event) {
        fnDeleteCharacter(this);
    });

    var fnDeleteCharacter = function(elem) {
        console.log('elemento', elem, $(elem).parents('.list_movies--row'));
        $(elem).parents('.list_movies--row').remove();
    }
});