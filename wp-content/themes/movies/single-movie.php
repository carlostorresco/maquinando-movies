<?php
get_header();
?>
<main id="main" class="site-main">
    <div class="container-fluid">
        <?php
        while ( have_posts() ){
            the_post();
            get_template_part('partials/content/single', 'movie');
        }
        ?>
    </div>
</main>
<?php
get_footer();