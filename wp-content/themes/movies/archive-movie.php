<?php
get_header();

$args = array(
    'post_type' => 'movie',
    'orderby'   => 'rand',
    'posts_per_page' => 3, 
    );
 
$featured = new WP_Query( $args );

?>
<main id="main" class="site-main">
    <div class="container-fluid">
        <section class="featured-list m-auto">
            <h4 class="featured-list--title">Peliculas destacadas</h4>
            <div class="row">
            <?php 
                if ( $featured->have_posts() ) {
                    while ( $featured->have_posts() ) {
                        $featured->the_post();
                        $score = get_post_meta(get_the_ID(), 'input_score', true);?>
                        <div class="col-sm">
                            <div class="card">
                                <a href="<?php the_permalink() ?>">
                                    <img class="card-img-top" src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                                </a>
                                <div class="card-body">
                                    <div class="score">
                                        <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" class="ipc-icon ipc-icon--star-inline" viewBox="0 0 24 24" fill="currentColor" role="presentation">
                                            <path d="M12 20.1l5.82 3.682c1.066.675 2.37-.322 2.09-1.584l-1.543-6.926 5.146-4.667c.94-.85.435-2.465-.799-2.567l-6.773-.602L13.29.89a1.38 1.38 0 0 0-2.581 0l-2.65 6.53-6.774.602C.052 8.126-.453 9.74.486 10.59l5.147 4.666-1.542 6.926c-.28 1.262 1.023 2.26 2.09 1.585L12 20.099z"></path>
                                        </svg>
                                        <span><?php echo $score ?></span>
                                    </div>
                                    <h5 class="card-title"><?php the_title() ?></h5>
                                    <a href="<?php the_permalink() ?>" class="btn view-more">Ver más</a>
                                </div>
                            </div>
                        </div>
            <?php   }
                }
            ?>
            </div>
            <div class="row">
                <div class="col-sm">
                    <a href="#" class="btn all-movies">Todas las peliculas</a>
                </div>
            </div>
        </section>
        <section class="maquinando-list">
            <div class="row">
            <?php
            if (have_posts()) :
                while (have_posts()) :the_post();
                    get_template_part('partials/content/content', 'movie');
                endwhile;
            endif;
            ?>
            </div>
        </section>
    </div>
</main>
<?php
get_footer();
