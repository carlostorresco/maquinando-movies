<?php 

function admin_styles() {
    wp_enqueue_style( 'css-admin', get_theme_file_uri('/assets/css/admin-general.css'), array(), null );
    wp_enqueue_script( 'nutriendocreciendo-js-admin', get_theme_file_uri( '/assets/js/admin-general.js' ), array( 'jquery' ), '1.0', true );
}
add_action( 'admin_enqueue_scripts', 'admin_styles' );