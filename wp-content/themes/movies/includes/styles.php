<?php
function app_css_scripts()
{
    wp_enqueue_style(
        'bootstrap_css',
        'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
        array(),
        '4.0.0'
    );

    wp_enqueue_style( 'css-app', get_theme_file_uri('/assets/css/app.css'), array(), null );
    wp_enqueue_script( 'js-app', get_theme_file_uri( '/assets/js/app.js' ), array( 'jquery' ), '1.0', true );
}

add_action('wp_enqueue_scripts', 'app_css_scripts');
