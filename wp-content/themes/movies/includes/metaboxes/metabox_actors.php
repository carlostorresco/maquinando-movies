<?php
function create_actor_metabox()
{
    add_meta_box(
        'metabox_actors',
        'Participación',
        'callback_actor_metabox',
        'actor',
        'normal',
        'high'
    );
}

function callback_actor_metabox($post)
{
    $movie = get_post_meta($post->ID, 'input_movie_id', true);
    $characters = get_post_meta($post->ID, 'input_character', true);
    
    wp_nonce_field('event_metabox_nonce', 'events_nonce');

    $args = array('post_type' => 'movie');
    $query = new WP_Query($args);

?>
    <div class="custom_metabox" style="width: 100%;">

        <div class="custom_metabox_field">
            <div class="custom_desc">
                <label>Reparto</label>
                <p>Seleccione los actores y su correspondiente personaje</p>
            </div>
            <div class="custom_field custom_field__button">
                <select name="input_movie[]" id="input_movie">
                    <?php
                    if ($query->have_posts()) {
                        while ($query->have_posts()) {
                            $query->the_post();
                            echo ('<option value="' . get_the_ID() . '">' . trim(get_the_title()) . '</option>');
                        }
                    }
                    ?>
                </select>
                <button type="button" id="addMovie" class="button-add"><i class="dashicons dashicons-plus"></i></button>
                <div class="list_movies">
                    <?php 
                        foreach ($characters as $key=>$character) { 
                            $args = array('p' => $movie[$key], 'post_type' => 'movie');
                            $loop = new WP_Query($args);
                            ?>
                            <div class="list_movies--row">
                                <input type="hidden" name="input_movie_id[]" value="<?php echo $movie[$key] ?>" />
                                <div class="list_movies--movie">
                                    <span class="title">Película:</span> 
                                    <span class="name">
                                        <?php
                                        if ($loop->have_posts()) {
                                            while ($loop->have_posts()) {
                                                $loop->the_post();
                                                the_title();
                                            }
                                        }
                                        ?>
                                    </span>
                                </div>
                                <div class="list_movies--character">
                                    <span class="title">Nombre del personaje:</span> 
                                    <input type="text" name="input_character[]" placeholder="Nombre del personaje"  value="<?php echo $character ?>"/>
                                </div>
                                <a href="javascript:;" id="removeCharacter" class="list_movies--delete"><i class="dashicons dashicons-no"></i></a>
                            </div>
                    <?php 
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
<?php
}

add_action('add_meta_boxes', 'create_actor_metabox');


function save_actor_metabox($post_id){
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( !current_user_can( 'edit_post', $post_id )) return;
    if( !isset( $_POST['events_nonce'] ) || !wp_verify_nonce( $_POST['events_nonce'],'event_metabox_nonce') ) return;

    $fields = ['input_movie_id','input_character'];
    
    foreach ( $fields as $field ) {
        if ( array_key_exists( $field, $_POST ) ) {
            update_post_meta( $post_id, $field, $_POST[$field]);
        }
    }
}

add_action('save_post', 'save_actor_metabox');