<?php
function create_movie_metabox()
{
    add_meta_box(
        'metabox_movies',
        'Detalles de la película',
        'callback_movie_metabox',
        'movie',
        'normal',
        'high'
    );
}

function callback_movie_metabox($post)
{
    $movie_score = get_post_meta($post->ID, 'input_score', true);
    $movie_actors = get_post_meta($post->ID, 'input_actor', true);
    
    wp_nonce_field('event_metabox_nonce', 'events_nonce');

    $args = array(
        'post_type' => 'actor'
    );
    $query = new WP_Query($args);
?>
    <div class="custom_metabox" style="width: 100%;">

        <div class="custom_metabox_field">
            <div class="custom_desc">
                <label>Calificación</label>
                <p>Determine un rango de 1 a 10</p>
            </div>
            <div class="custom_field">
                <input type="number" min="1" max="10" step='0.5' placeholder="1" name="input_score" id="input_score" class="field-small" value="<?php echo ($movie_score)  ?>" />
            </div>
        </div>

        <div class="custom_metabox_field">
            <div class="custom_desc">
                <label>Reparto</label>
                <p>Seleccione los actores</p>
            </div>
            <div class="custom_field">
                <select name="input_actor[]" id="input_actor" class="multiple" multiple>
                    <?php
                    if ($query->have_posts()) {
                        while ($query->have_posts()) {
                            $query->the_post();
                            $is_selected = (in_array(get_the_ID(), $movie_actors) ? 'selected' : '');
                            echo ('<option value="' . get_the_ID() . '" '.$is_selected.'>' . get_the_title() . '</option>');
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="clear"></div>
<?php
}

add_action('add_meta_boxes', 'create_movie_metabox');


function save_movie_metabox($post_id){
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( !current_user_can( 'edit_post', $post_id )) return;
    if( !isset( $_POST['events_nonce'] ) || !wp_verify_nonce( $_POST['events_nonce'],'event_metabox_nonce') ) return;

    $fields = ['input_score','input_actor'];

    /*if($_POST['input_actor']){
        $data = (array) $_POST['input_actor'];
        echo('Data');
        var_dump($_POST['input_actor']);
        var_dump($data);
        exit();
    }*/
    
    foreach ( $fields as $field ) {
        if ( array_key_exists( $field, $_POST ) ) {
            update_post_meta( $post_id, $field, $_POST[$field]);
        }
    }
}

add_action('save_post', 'save_movie_metabox');