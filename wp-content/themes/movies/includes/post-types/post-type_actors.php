<?php

function create_actor_posttype(){
	$args = array(
		'label' => 'Actor',
		'labels' => array(
			'name' => __('Actor'),
			'singular_name' => __('Actor'),
			'add_new' => __('Agregar nuevo'),
			'add_new_item' => __('Agregar nuevo Actor'),
			'edit_item' => __('Editar Actor'),
			'new_item' => __('Nuevo Actor'),
			'view_item' => __('Ver Actor'),
			'search_items' => __('Búscar Actores'),
			'not_found' => __('Actor no encontrado'),
			'not_found_in_trash' => __('Actor no encontrado en la papelera'),
			'all_items' => __('Todos los Actores'),
			'archives' => __('Registro de Actores'),
			'insert_into_item' => __('Insertar en Actor'),
			'uploaded_to_this_item' => __('Cargar a este Actor'),
			'featured_image' => __('Imagen Actor'),
			'set_featured_image' => __('Seleccionar imagen'),
			'remove_featured_image' => __('Eliminar Imagen'),
			'use_featured_image' => __('Usar Imagen'),
			'menu_name' => __('Actores')
		),
		'description' => __('Actores Maquinando.'),
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 18,
		'menu_icon' => 'dashicons-universal-access',
        'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
		'has_archive' => true,
		'hierarchical' => false,
		'rewrite' => array('slug'=>'actores','with_front'=>false),
		'capability_type' => 'page',
	);
	register_post_type('actor',$args);
}
add_action('init','create_actor_posttype',0);
