<?php

function create_movie_posttype(){
	$args = array(
		'label' => 'Película',
		'labels' => array(
			'name' => __('Película'),
			'singular_name' => __('Película'),
			'add_new' => __('Agregar nuevo'),
			'add_new_item' => __('Agregar nueva Película'),
			'edit_item' => __('Editar Película'),
			'new_item' => __('Nueva Película'),
			'view_item' => __('Ver Película'),
			'search_items' => __('Búscar Películas'),
			'not_found' => __('Película no encontrada'),
			'not_found_in_trash' => __('Película no encontrada en la papelera'),
			'all_items' => __('Todas las Películas'),
			'archives' => __('Registro de Películas'),
			'insert_into_item' => __('Insertar en Película'),
			'uploaded_to_this_item' => __('Cargar a esta Película'),
			'featured_image' => __('Imagen Película'),
			'set_featured_image' => __('Seleccionar imagen'),
			'remove_featured_image' => __('Eliminar Imagen'),
			'use_featured_image' => __('Usar Imagen'),
			'menu_name' => __('Películas')
		),
		'description' => __('Peliculas Maquinando.'),
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 18,
		'menu_icon' => 'dashicons-tickets-alt',
        'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
		'has_archive' => true,
		'hierarchical' => false,
		'rewrite' => array('slug'=>'peliculas','with_front'=>false),
		'capability_type' => 'page',
	);
	register_post_type('movie',$args);
}
add_action('init','create_movie_posttype',0);
