<?php 
/* -------------------------------
/ Register wp_nav_menu()
/  ------------------------------- */
register_nav_menus(
    array(
        'menu-header' => __( 'Top Header', 'maquinando' )
    )
);

/* -------------------------------
/ Disable Gutenberg
/  ------------------------------- */
add_filter('use_block_editor_for_post', '__return_false', 10);

/* -------------------------------
/ add theme support for thumbnails
/  ------------------------------- */
add_theme_support(
	'post-thumbnails',
	array(
		'post',
		'page',
        'movie',
        'actor'
	)
);