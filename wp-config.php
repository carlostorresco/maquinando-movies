<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_maquinando' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'LJz4l8Tk:#J3GI,*z?*jB#v52^Qb;ZMm#N!LPVaChav*!+KQ2]`%)w_0+<LD5Q54' );
define( 'SECURE_AUTH_KEY',  '#wg.R l0*)tzag<[Tf@V{yo]{QM49DzYQ8W8-^qeG-Pt(*QvB;(DcieQ{oih[4WU' );
define( 'LOGGED_IN_KEY',    'cd=JtMqEV&>U|3j~29_k/YxfJctO?l#(U80_H`}7aLl.?tD2!Mg&9{Xwk.|PMw}j' );
define( 'NONCE_KEY',        'FKCn>D66J^A-40H{4p_3utd`_ dmZ:MQ#/CgX^6Ou+LUDDWQBWKTrOLRcG-/EgG9' );
define( 'AUTH_SALT',        'NT70y.hiM3f:;8rHRDLei28#3P.`&ya*-xiyg5U,GnL8I!A(!im{y]mM|>:LoYO#' );
define( 'SECURE_AUTH_SALT', '||jH[g9}W(XrUW=0s3A`t-,BN8W,;}<bN[nRfHA9o#:a{-E;)X!3QzD#Gk|3`mEz' );
define( 'LOGGED_IN_SALT',   '=}[&`;%7L3:0OwUpOB$7vrxi|eHg4w.eDyJ1KXV:aKx7gEc~ vih|7S7,$Gm1E;y' );
define( 'NONCE_SALT',       'vnn;ivNQ?cf^Jh5230O4v!.]$>z*)NHon_Dey>I@EInD*FZE.S9%~fKh /zfwrcE' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
